# Unity.IDisposableTrackingExtension #

This is an extension for the [Unity](https://github.com/unitycontainer/unity)
IoC container that enables tracking and returning IDisposable instances that are
created during a Resolve() call, in order to dispose of them cleanly.

## Quick start ##

The package can be installed from [Nuget.org](https://www.nuget.org/packages/Unity.IDisposableTrackingExtension).
Configuring it for your container is simple:

    container.AddNewExtension< IDisposableTrackingExtension >();

Then you can use the provided `ResolveForDisposal()` extension methods to
resolve types in a way that returns the requested type wrapped in an object that
keeps track of all of the IDisposable instances created during the resolve call.
That object is disposable itself, and so can be disposed as normal, in turn
disposing all of the IDisposable instances being tracked.

    using ( var disposableWrapper = container.ResolveForDisposal< YourType >() )
    {
        YourType subject = disposableWrapper.Subject;
        ... do stuff
    }

If no IDisposable instances are created during resolution, using
`ResolveForDisposal()` still works fine, the wrapper object will simply do
nothing when disposed.

In fact, it's recommended to switch to `ResolveForDisposal()` for all type
resolution, for two reasons:

 1. You'll be protected in the future if the object graph ever changes and
    IDisposable instances _are_ created.

 2. Part of the extension's operation is to throw an exception if an IDisposable
    instance is created and not tracked, which is what will happen when using
    the normal `Resolve()` functions. This is meant as a safety feature. If an
    untracked IDisposable is created, the exception will make it obvious and the
    code can be fixed, by changing to `ResolveForDisposal()` and disposing
    correctly.

## Motivation ##

When not using Unity, IDisposable instances have a well-understood usage
pattern:

1. Within a function, put them in a `using` block to get disposal "for free".

2. When created for an instance member of a class, implement IDisposable in the
   class and put clean-up in `Dispose()`.

3. When passed into a class's constructor, do nothing, as the IDisposable
   instance is owned somewhere else.

Unity confuses things because when dependency injection is done properly, case #2
above goes away. All dependencies should be injected, which means essentially
no classes will have ownership of the IDisposable instances being created.
However, neither does it provide a way to get at the IDisposables that were
created during a `Resolve()` call, so it seems that `using` blocks can't be
used. What option is left?

My conclusion is that the `Resolve()` interface is essentially wrong. Returning
only the requested type and leaking objects that need special handling like
IDisposable can't be correct.

In response, I wrote this extension for Unity, which tracks IDisposable
instances created during a type resolution, and returns a disposable wrapper
object containing an instance of the requested type and all of the IDisposable
dependencies from the object graph. This reconciles IDisposable usage patterns #1
and #3 from above. Normal classes use dependency injection; they don't own
injected IDisposables, so they don't dispose of them. Classes that perform type
resolution  because they need dynamically created objects, those classes are
the owners, and this extension provides the facility for managing disposal
scopes.
