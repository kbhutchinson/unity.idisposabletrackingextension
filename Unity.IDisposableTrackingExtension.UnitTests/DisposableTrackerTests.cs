using System;
using NUnit.Framework;

namespace Unity.IDisposableTrackingExtension.UnitTests
{
    public class DisposableTrackerTests
    {
        [Test]
        public void Verify_DisposeSetsTrue_Assumption()
        {
            var dst = new DisposeSetsTrue();
            Assert.That( dst.Disposed, Is.False );
            dst.Dispose();
            Assert.That( dst.Disposed, Is.True );
        }

        [Test]
        public void DisposableTracker_SupportsInitializerSyntax()
        {
            /*** Arrange ***/

            var dst1 = new DisposeSetsTrue();
            var dst2 = new DisposeSetsTrue();
            var tracker = new DisposableTracker
            {
                dst1,
                dst2,
            };

            /*** Act ***/

            tracker.Dispose();

            /*** Assert ***/

            Assert.That( dst1.Disposed, Is.True );
            Assert.That( dst2.Disposed, Is.True );
        }
    }
}
