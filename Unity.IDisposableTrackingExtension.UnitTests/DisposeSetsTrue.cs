//-----------------------------------------------------------------------
// <copyright company="Thomson Reuters">
//     Copyright 2016: Thomson Reuters. All Rights Reserved. Proprietary and Confidential information of Thomson Reuters. Disclosure, Use or Reproduction without the written authorization of Thomson Reuters is prohibited.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace Unity.IDisposableTrackingExtension.UnitTests
{
    /// <summary>
    /// Simple class that sets an internal member to true when disposed so disposal can be determined
    /// </summary>
    internal class DisposeSetsTrue : IDisposable
    {
        private bool disposed = false;

        public bool Disposed { get { return disposed; } }

        public void Dispose()
        {
            disposed = true;
        }
    }

}
