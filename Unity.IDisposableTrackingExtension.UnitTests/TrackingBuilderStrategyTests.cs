using System;
using Microsoft.Practices.Unity;
using NUnit.Framework;

namespace Unity.IDisposableTrackingExtension.UnitTests
{
    public class TrackingBuilderStrategyTests
    {
        [Test]
        public void TrackingBuilderStrategy_SupportsOverridingTracker_WithRegisteredInstance()
        {
            /*** Arrange ***/

            var dst1 = new DisposeSetsTrue();
            var tracker = new DisposableTracker { dst1 };
            var unity = new UnityContainer().AddNewExtension< IDisposableTrackingExtension >();
            unity.RegisterInstance< IDisposableTracker >( tracker );

            var wrapper = unity.ResolveForDisposal< DisposeSetsTrue >();
            var dst2 = wrapper.Subject;

            Assert.That( dst1.Disposed, Is.False );
            Assert.That( dst2.Disposed, Is.False );

            /*** Act ***/

            wrapper.Dispose();

            /*** Assert ***/

            Assert.That( dst1.Disposed, Is.True );
            Assert.That( dst2.Disposed, Is.True );
        }

        [Test]
        public void TrackingBuilderStrategy_SupportsOverridingTracker_WithDependencyOverride()
        {
            /*** Arrange ***/

            var dst1 = new DisposeSetsTrue();
            var tracker = new DisposableTracker { dst1 };
            var unity = new UnityContainer().AddNewExtension< IDisposableTrackingExtension >();

            var wrapper = unity.ResolveForDisposal< DisposeSetsTrue >( new DependencyOverride< IDisposableTracker >( tracker ) );
            var dst2 = wrapper.Subject;

            Assert.That( dst1.Disposed, Is.False );
            Assert.That( dst2.Disposed, Is.False );

            /*** Act ***/

            wrapper.Dispose();

            /*** Assert ***/

            Assert.That( dst1.Disposed, Is.True );
            Assert.That( dst2.Disposed, Is.True );
        }
    }
}
