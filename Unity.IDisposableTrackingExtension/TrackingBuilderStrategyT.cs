//-----------------------------------------------------------------------
// <copyright>
//     Copyright 2016: Kurt Hutchinson. All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Microsoft.Practices.ObjectBuilder2;

namespace Unity.IDisposableTrackingExtension
{
    /// <summary>
    /// Builder strategy that can track object creation using an instance in the container
    /// </summary>
    /// <typeparam name="TTracker"> Type of the tracker being used </typeparam>
    public class TrackingBuilderStrategy< TTracker > : BuilderStrategy
        where TTracker : class
    {
        private readonly Action< TTracker, object > trackingAction;

        /// <summary>
        /// Initializes a new instance of the <see cref="TrackingBuilderStrategy{T}"/> class
        /// </summary>
        /// <param name="trackingAction"> Action to be performed on container-created objects </param>
        public TrackingBuilderStrategy( Action< TTracker, object > trackingAction )
        {
            if ( trackingAction == null )
            {
                throw new ArgumentNullException( "trackingAction" );
            }

            this.trackingAction = trackingAction;
        }

        /// <summary>
        /// Called during the chain of responsibility for a build operation. The PostBuildUp
        /// method is called when the chain has finished the PreBuildUp phase and executes
        /// in reverse order from the PreBuildUp calls.
        /// </summary>
        /// <param name="context"> Context of the build operation </param>
        public override void PostBuildUp( Microsoft.Practices.ObjectBuilder2.IBuilderContext context )
        {
            base.PostBuildUp( context );

            if ( context.Existing is TTracker )
            {
                // If a TTracker was built by the container but the type in its BuildKey is not TTracker,
                // it's probably mapped to something else. However, we also need the policy registered
                // against the type TTracker so we can find it later.
                if ( context.BuildKey.Type != typeof(TTracker) )
                {
                    var lifetimePolicy = context.Policies.Get< ILifetimePolicy >( context.BuildKey );
                    if ( lifetimePolicy == null )
                        throw new InvalidOperationException( string.Format( "Existing {0} has no associated lifetime policy.", typeof(TTracker).Name ) );

                    context.Policies.Set< ILifetimePolicy >( lifetimePolicy, NamedTypeBuildKey.Make< TTracker >() );
                }
            }
            else
            {
                TTracker tracker = null;

                // First check for a dependency override for the tracker.
                var resolverPolicy = context.GetOverriddenResolver( typeof(TTracker) );
                if ( resolverPolicy != null )
                {
                    tracker = resolverPolicy.Resolve( context ) as TTracker;
                }
                else
                {
                    // If not overridden, we should be able to look up the tracker against the lifetime policy.
                    var lifetimePolicy = context.Policies.Get< ILifetimePolicy >( NamedTypeBuildKey.Make< TTracker >() );
                    if ( lifetimePolicy != null )
                    {
                        tracker = lifetimePolicy.GetValue() as TTracker;
                    }
                }

                // A tracker may or may not have been found, but it's really up to the registered action to
                // handle the one situation or the other.
                trackingAction( tracker, context.Existing );
            }
        }
    }
}
