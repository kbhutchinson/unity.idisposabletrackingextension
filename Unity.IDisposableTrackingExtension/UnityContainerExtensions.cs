//-----------------------------------------------------------------------
// <copyright>
//     Copyright 2016: Kurt Hutchinson. All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Microsoft.Practices.Unity;

namespace Unity.IDisposableTrackingExtension
{
    /// <summary>
    /// Extension methods for resolving disposable dependency bundles
    /// </summary>
    public static class UnityContainerExtensions
    {
        /// <summary>
        /// Resolve a type, tracking and returning <see cref="IDisposable"/> instances created during the resolve, so they can be disposed.
        /// </summary>
        /// <typeparam name="T"> The type requested </typeparam>
        /// <param name="container"> Container to resolve from </param>
        /// <param name="overrides"> Overrides for the resolve call </param>
        /// <returns> The resolved object, wrapped in a disposable container that will dispose any created <see cref="IDisposable"/> instances </returns>
        public static IDependencyDisposer< T > ResolveForDisposal< T >( this IUnityContainer container, params ResolverOverride[] overrides )
        {
            return container.Resolve< IDependencyDisposer< T > >( overrides );
        }

        /// <summary>
        /// Resolve a type, tracking and returning <see cref="IDisposable"/> instances created during the resolve, so they can be disposed.
        /// </summary>
        /// <typeparam name="T"> The type requested </typeparam>
        /// <param name="container"> Container to resolve from </param>
        /// <param name="name"> Name of the object to resolve </param>
        /// <param name="overrides"> Overrides for the resolve call </param>
        /// <returns> The resolved object, wrapped in a disposable container that will dispose any created <see cref="IDisposable"/> instances </returns>
        public static IDependencyDisposer< T > ResolveForDisposal< T >( this IUnityContainer container, string name, params ResolverOverride[] overrides )
        {
            return container.Resolve< IDependencyDisposer< T > >( name, overrides );
        }
    }
}
