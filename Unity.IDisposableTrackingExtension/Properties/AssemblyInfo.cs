using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Unity.IDisposableTrackingExtension")]
[assembly: AssemblyDescription("Unity container extension to track IDisposable instances for easy disposal")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("")]
[assembly: AssemblyProduct("Unity.IDisposableTrackingExtension")]
[assembly: AssemblyCopyright("Copyright ©  2016")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("e42976be-7635-41a4-98a5-2762f36f0a17")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Patch Version
//
[assembly: AssemblyVersion("2.0.0")]
[assembly: AssemblyFileVersion("2.0.0")]
