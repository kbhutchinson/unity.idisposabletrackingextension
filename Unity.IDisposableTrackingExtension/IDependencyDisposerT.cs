//-----------------------------------------------------------------------
// <copyright>
//     Copyright 2016: Kurt Hutchinson. All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;

namespace Unity.IDisposableTrackingExtension
{
    /// <summary>
    /// Interface that represents a disposable bundle of disposable dependencies of the subject instance
    /// </summary>
    /// <typeparam name="T"> Type of the subject instance </typeparam>
    public interface IDependencyDisposer< out T > : IDisposable
    {
        /// <summary>
        /// Gets the subject instance
        /// </summary>
        T Subject { get; }
    }

    /// <summary>
    /// Implementation of a disposable bundle of disposable dependencies of a subject instance
    /// </summary>
    /// <typeparam name="T"> Type of the subject instance </typeparam>
    public class DependencyDisposer< T > : IDependencyDisposer< T >
    {
        private readonly IDisposableTracker tracker;
        private bool disposing = false;

        /// <summary>
        /// Initializes a new instance of the <see cref="DependencyDisposer{T}"/> class
        /// </summary>
        public DependencyDisposer( IDisposableTracker tracker )
        {
            this.tracker = tracker;
        }

        /// <summary>
        /// Gets or sets the subject instance
        /// </summary>
        public T Subject { get; set; }

        /// <summary>
        /// Disposes of the tracked disposable dependencies
        /// </summary>
        public virtual void Dispose()
        {
            // avoid recursion
            if ( disposing )
            {
                return;
            }

            disposing = true;
            tracker.Dispose();
        }
    }
}
