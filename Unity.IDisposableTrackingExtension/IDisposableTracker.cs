//-----------------------------------------------------------------------
// <copyright>
//     Copyright 2016: Kurt Hutchinson. All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Unity.IDisposableTrackingExtension
{
    /// <summary>
    /// Interface for tracking <see cref="IDisposable"/> instances
    /// </summary>
    public interface IDisposableTracker : IDisposable
    {
        /// <summary>
        /// Add an <see cref="IDisposable"/> instance to the tracker
        /// </summary>
        /// <param name="disposable"> <see cref="IDisposable"/> instance to begin tracking </param>
        void Add( IDisposable disposable );
    }

    /// <summary>
    /// Implementation of tracking <see cref="IDisposable"/> instances
    /// </summary>
    public class DisposableTracker : IDisposableTracker, IEnumerable< IDisposable >
    {
        private readonly List< IDisposable > disposables = new List< IDisposable >();
        private bool disposing = false;

        /// <summary>
        /// Function to be used as the tracker action for a <see cref="TrackingBuilderStrategy{T}"/>
        /// </summary>
        /// <param name="tracker"> Tracker to add to </param>
        /// <param name="candidate"> Candidate object for tracking </param>
        public static void Add( IDisposableTracker tracker, object candidate )
        {
            var disposable = candidate as IDisposable;
            if ( disposable == null )
            {
                return;
            }

            if ( tracker == null )
            {
                throw new InvalidOperationException( "IDisposable tracking requires a registered IDisposableTracker. Did you call Resolve() instead of ResolveForDisposal()?" );
            }

            tracker.Add( disposable );
        }

        /// <summary>
        /// Add an <see cref="IDisposable"/> instance to the tracker
        /// </summary>
        /// <param name="disposable"> <see cref="IDisposable"/> instance to begin tracking </param>
        public void Add( IDisposable disposable )
        {
            disposables.Add( disposable );
        }

        /// <summary>
        /// Disposes of tracked <see cref="IDisposable"/> instances
        /// </summary>
        public virtual void Dispose()
        {
            // avoid recursion
            if ( disposing )
            {
                return;
            }

            disposing = true;
            ChainedDispose( disposables );
        }

        /// <summary>
        /// Dispose of a sequence of <see cref="IDisposable"/> instances
        /// </summary>
        /// <param name="disposables"> Sequence of <see cref="IDisposable"/> instances to dispose </param>
        private static void ChainedDispose( IEnumerable< IDisposable > disposables )
        {
            if ( disposables == null || ! disposables.Any() )
            {
                return;
            }

            try
            {
                disposables.First().Dispose();
            }
            finally
            {
                ChainedDispose( disposables.Skip( 1 ) );
            }
        }

        /// <summary>
        /// Returns an enumerator that iterates through the collection.
        /// </summary>
        /// <returns> A <see cref="System.Collections.Generic.IEnumerator{T}"/> that can be used to iterate through the collection. </returns>
        public IEnumerator< IDisposable > GetEnumerator()
        {
            return disposables.GetEnumerator();
        }

        /// <summary>
        /// Returns an enumerator that iterates through a collection.
        /// </summary>
        /// <returns> A <see cref="IEnumerator"/> object that can be used to iterate through the collection. </returns>
        IEnumerator IEnumerable.GetEnumerator()
        {
            return disposables.GetEnumerator();
        }
    }
}
