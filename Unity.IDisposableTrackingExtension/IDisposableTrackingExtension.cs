//-----------------------------------------------------------------------
// <copyright>
//     Copyright 2016: Kurt Hutchinson. All Rights Reserved.
// </copyright>
//-----------------------------------------------------------------------

using System;
using Microsoft.Practices.Unity;

namespace Unity.IDisposableTrackingExtension
{
    /// <summary>
    /// Extension to <see cref="IUnityContainer"/> that tracks <see cref="IDisposable"/> instances
    /// created by the container during a Resolve(), in order to make them available for disposal.
    /// </summary>
    public class IDisposableTrackingExtension : UnityContainerExtension
    {
        /// <summary>
        /// Initialize the container with this extension's functionality
        /// </summary>
        protected override void Initialize()
        {
            var strategy = new TrackingBuilderStrategy< IDisposableTracker >( DisposableTracker.Add );
            Context.Strategies.Add( strategy, Microsoft.Practices.Unity.ObjectBuilder.UnityBuildStage.PostInitialization );
            Context.Container
                .RegisterType< IDisposableTracker, DisposableTracker >( new PerResolveLifetimeManager() )
                .RegisterType( typeof(IDependencyDisposer<>), typeof(DependencyDisposer<>), new InjectionProperty( "Subject" ) )
                ;
        }
    }
}
